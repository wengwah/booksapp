const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");

// create instance of express
const app = express();

// Sanitize req and res data where possible
app.use(bodyParser.urlencoded( { extended: false }));
app.use(bodyParser.json());

// Define static paths
const CLIENT_FOLDER = path.join(__dirname, "../client");
app.use("/", express.static(CLIENT_FOLDER));

const BOWER_COMPONENTS = path.join(__dirname, "../bower_components");
app.use("/libs", express.static(BOWER_COMPONENTS));


var port = process.env.PORT || 3000;

app.listen(port, function() {
  console.log("Web app started at port %d", port);
});

// Make this app available for public eg. testing
module.exports = app;