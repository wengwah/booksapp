(function () {
    "use strict";
    angular.module("BooksApp").config(BooksConfig);
    MyConfig.$inject = ["$stateProvider", "$urlRouterProvider"];
    
    function BooksConfig($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state("overview", {
                url: "/overview",
                templateUrl: "/views/overview.html",
                controller: "OverviewCtrl as overviewCtrl"
            })
            .state("reviews", {
                url: "/reviews/:isbn", // tbc
                templateUrl: "/views/reviews.html", // tbc
                controller: "ReviewsCtrl as reviewCtrl" // tbc
            })

        $urlRouterProvider.otherwise("/overview");

    } 
    
})();