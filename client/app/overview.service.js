(function() {
  angular.module("BooksApp").service("OverviewSvc", OverviewSvc);

  OverviewSvc.$inject = ["$http", "$q"];

  function OverviewSvc($http, $q) {
    var overviewSvc = this; // overviewSvc

    // Instantiating functions
    overviewSvc.getBooks = getBooks;

    // New York Times Books API params
    var nytURL = "https://api.nytimes.com/svc/books/v3/lists/overview.json";
    var nytKey = "7b8767561d6a4e9da597afb88b65557c";

    function getBooks() {
      var defer = $q.defer();

      $http.get(nytURL, {
        params: {
          "api-key": nytKey
        }
      }).then(function (result) {
        // the following lines appear to be unnecessary
        // var holdme = result.data.results.lists; 
        // defer.resolve(holdme);
        defer.resolve(result.data.results.lists);
      }).catch (function (error) {
        defer.reject(error);
      })
      return (defer.promise);
    
    }

  }

}) ();