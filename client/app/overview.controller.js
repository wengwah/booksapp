(function () {
  "use strict";
  angular.module("BooksApp").controller("OverviewCtrl", OverviewCtrl);

  OverviewCtrl.$inject = ["$state", "OverviewSvc"];

  function OverviewCtrl($state, OverviewSvc) {
    var overviewCtrl = this; // vm

    // Commented away unnecessary portions meant for a later version
    // overviewCtrl.holdme = {}; // array for holding with huge ass nested json objects
    // overviewCtrl.categoryNames = []; // array for holding book category names (strings)
    // overviewCtrl.titles = []; // array for holding book titles
    // overviewCtrl.showResults = false; // boolean for toggling ng-hide and ng-show

    // function initPage() {
    //   OverviewSvc.getBooks()
    //     .then(function (result) {
    //       vm.holdme = overviewSvc.holdme;
    //       // result.results.lists; // stores array with huge ass json objects
    //       for (var i in vm.holdme.lists) {
    //         vm.categoryNames.push(vm.holdme.lists[i].list_name);
    //       }
    //       vm.showResults = true;
    //       console.log(vm.holdme);
    //       console.log("--------------");
    //       console.log(vm.categoryNames);
    //     }).catch (function (error) {
    //       console.log(error);
    //     });
    // }

    // vm.initPage();

    OverviewSvc.getBooks()
      .then(function(categories) {
        overviewCtrl.categories = categories;
      });

  }

  BooksApp.config(BooksConfig);
  BooksApp.service("OverviewSvc", OverviewSvc);

  BooksApp.controller("OverviewCtrl", OverviewCtrl);

}) ();