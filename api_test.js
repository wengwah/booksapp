var nyt = {
    "status": "OK",
    "copyright": "Copyright (c) 2017 The New York Times Company.  All Rights Reserved.",
    "num_results": 70,
    "results": {
        "bestsellers_date": "2017-09-09",
        "published_date": "2017-09-24",
        "published_date_description": "latest",
        "previous_published_date": "2017-09-17",
        "next_published_date": "",
        "lists": [
            {
                "list_id": 704,
                "list_name": "Combined Print and E-Book Fiction",
                "list_name_encoded": "combined-print-and-e-book-fiction",
                "display_name": "Combined Print & E-Book Fiction",
                "updated": "WEEKLY",
                "list_image": "https://s1.nyt.com/du/books/images/9781250123152.jpg",
                "list_image_width": 326,
                "list_image_height": 495,
                "books": [
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/Secrets-Death-Dallas-Novel-Book/dp/1250123151?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "JD Robb",
                        "book_image": "https://s1.nyt.com/du/books/images/9781250123152.jpg",
                        "book_image_width": 326,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by J.D. Robb",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:06",
                        "description": "Lt. Eve Dallas investigates the murder of a professional gossip who dabbled in blackmail; by Nora Roberts, writing pseudonymously.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "None",
                        "primary_isbn13": "9781250123183",
                        "publisher": "St. Martin's",
                        "rank": 1,
                        "rank_last_week": 0,
                        "sunday_review_link": "",
                        "title": "SECRETS IN DEATH",
                        "updated_date": "2017-09-16 22:00:07",
                        "weeks_on_list": 1,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/Secrets-Death-Dallas-Novel-Book/dp/1250123151?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9781250123183?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781250123183"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/Enemy-State-Mitch-Rapp-Novel-ebook/dp/B06ZZBDNYP?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Kyle Mills",
                        "book_image": "https://s1.nyt.com/du/books/images/9781476783543.jpg",
                        "book_image_width": 322,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by Kyle Mills",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:06",
                        "description": "Vince Flynn's character Mitch Rapp leaves the C.I.A. to go on a manhunt when the nephew of a Saudi King finances a terrorist group.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "1476783543",
                        "primary_isbn13": "9781476783543",
                        "publisher": "Emily Bestler/Atria",
                        "rank": 2,
                        "rank_last_week": 0,
                        "sunday_review_link": "",
                        "title": "ENEMY OF THE STATE",
                        "updated_date": "2017-09-16 22:00:07",
                        "weeks_on_list": 1,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/Enemy-State-Mitch-Rapp-Novel-ebook/dp/B06ZZBDNYP?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9781476783543?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781476783543"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/Legacy-Spies-Novel-John-Carré/dp/0735225117?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "John le Carré",
                        "book_image": "https://s1.nyt.com/du/books/images/9780735225114.jpg",
                        "book_image_width": 328,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by John le Carré",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:06",
                        "description": "Peter Guillam, formerly of the British Secret Service, is pulled out of retirement to defend intelligence operations during the cold war that resulted in the deaths of people close to him.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "0735225117",
                        "primary_isbn13": "9780735225114",
                        "publisher": "Viking",
                        "rank": 3,
                        "rank_last_week": 0,
                        "sunday_review_link": "",
                        "title": "A LEGACY OF SPIES",
                        "updated_date": "2017-09-16 22:00:07",
                        "weeks_on_list": 1,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/Legacy-Spies-Novel-John-Carré/dp/0735225117?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780735225114?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780735225114"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/Stephen-King-ebook/dp/B018ER7K5I?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Stephen King",
                        "book_image": "https://s1.nyt.com/du/books/images/9781501141232.jpg",
                        "book_image_width": 325,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by Stephen King",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:06",
                        "description": "The collective clown phobias of seven teenagers are rekindled in their adult lives by the terrifying title character. Originally published in 1986.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "1501175467",
                        "primary_isbn13": "9781501175466",
                        "publisher": "Scribner",
                        "rank": 4,
                        "rank_last_week": 11,
                        "sunday_review_link": "",
                        "title": "IT",
                        "updated_date": "2017-09-16 22:00:07",
                        "weeks_on_list": 5,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/Stephen-King-ebook/dp/B018ER7K5I?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9781501175466?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781501175466"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/Legacy-Carpathian-Novel-Christine-Feehan/dp/0399583998?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Christine Feehan",
                        "book_image": "https://s1.nyt.com/du/books/images/9780399583995.jpg",
                        "book_image_width": 328,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by Christine Feehan",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:06",
                        "description": "Emeline Sanchez fights to recover from exchanging blood with a master vampire, even as she tries to heal the children she saved from him. A Carpathian novel.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "None",
                        "primary_isbn13": "9780399584008",
                        "publisher": "Berkley",
                        "rank": 5,
                        "rank_last_week": 0,
                        "sunday_review_link": "",
                        "title": "DARK LEGACY",
                        "updated_date": "2017-09-16 22:00:07",
                        "weeks_on_list": 1,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/Legacy-Carpathian-Novel-Christine-Feehan/dp/0399583998?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780399584008?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780399584008"
                            }
                        ]
                    }
                ]
            },
            {
                "list_id": 708,
                "list_name": "Combined Print and E-Book Nonfiction",
                "list_name_encoded": "combined-print-and-e-book-nonfiction",
                "display_name": "Combined Print & E-Book Nonfiction",
                "updated": "WEEKLY",
                "list_image": "https://s1.nyt.com/du/books/images/9780743247542.jpg",
                "list_image_width": 313,
                "list_image_height": 495,
                "books": [
                    {
                        "age_group": "",
                        "amazon_product_url": "http://www.amazon.com/The-Glass-Castle-A-Memoir/dp/074324754X?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Jeannette Walls",
                        "book_image": "https://s1.nyt.com/du/books/images/9780743247542.jpg",
                        "book_image_width": 313,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by Jeannette Walls",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:07",
                        "description": "The author recalls a bizarre childhood. Originally published in 2005 and the basis of the movie.",
                        "first_chapter_link": "https://www.nytimes.com/2005/03/13/books/chapters/0313-1st-walls.html",
                        "price": 0,
                        "primary_isbn10": "074324754X",
                        "primary_isbn13": "9780743247542",
                        "publisher": "Scribner",
                        "rank": 1,
                        "rank_last_week": 1,
                        "sunday_review_link": "https://www.nytimes.com/2005/03/13/books/review/013COVERPROSE.html",
                        "title": "THE GLASS CASTLE",
                        "updated_date": "2017-09-16 22:00:07",
                        "weeks_on_list": 55,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "http://www.amazon.com/The-Glass-Castle-A-Memoir/dp/074324754X?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780743247542?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780743247542"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/Astrophysics-People-Hurry-deGrasse-Tyson/dp/0393609391?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Neil deGrasse Tyson",
                        "book_image": "https://s1.nyt.com/du/books/images/9780393609394.jpg",
                        "book_image_width": 305,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by Neil deGrasse Tyson",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:07",
                        "description": "A straightforward, easy-to-understand introduction to the laws that govern the universe.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "0393609391",
                        "primary_isbn13": "9780393609394",
                        "publisher": "Norton",
                        "rank": 2,
                        "rank_last_week": 2,
                        "sunday_review_link": "",
                        "title": "ASTROPHYSICS FOR PEOPLE IN A HURRY",
                        "updated_date": "2017-09-16 22:00:07",
                        "weeks_on_list": 19,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/Astrophysics-People-Hurry-deGrasse-Tyson/dp/0393609391?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780393609394?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780393609394"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "http://www.amazon.com/Hillbilly-Elegy-Memoir-Family-Culture-ebook/dp/B0166ISAS8?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "JD Vance",
                        "book_image": "https://s1.nyt.com/du/books/images/9780062300560.jpg",
                        "book_image_width": 328,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by J.D. Vance",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:07",
                        "description": "A Yale Law School graduate looks at the struggles of the white working class through the story of his own childhood.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "0062300547",
                        "primary_isbn13": "9780062300546",
                        "publisher": "HarperCollins",
                        "rank": 3,
                        "rank_last_week": 3,
                        "sunday_review_link": "",
                        "title": "HILLBILLY ELEGY",
                        "updated_date": "2017-09-16 22:00:07",
                        "weeks_on_list": 59,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "http://www.amazon.com/Hillbilly-Elegy-Memoir-Family-Culture-ebook/dp/B0166ISAS8?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780062300546?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780062300546"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/Fantasyland-America-Haywire-500-Year-History/dp/1400067219?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Kurt Andersen",
                        "book_image": "https://s1.nyt.com/du/books/images/9781400067213.jpg",
                        "book_image_width": 328,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by Kurt Andersen",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:07",
                        "description": "The politics and culture of 21st-century America are put in the context of five centuries of historical events and movements, including elements of conspiracy theories, crackpot ideas and hucksterism.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "1400067219",
                        "primary_isbn13": "9781400067213",
                        "publisher": "Random House",
                        "rank": 4,
                        "rank_last_week": 0,
                        "sunday_review_link": "",
                        "title": "FANTASYLAND",
                        "updated_date": "2017-09-16 22:00:07",
                        "weeks_on_list": 1,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/Fantasyland-America-Haywire-500-Year-History/dp/1400067219?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9781400067213?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781400067213"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "http://www.amazon.com/Being-Mortal-Medicine-What-Matters/dp/0805095152?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Atul Gawande",
                        "book_image": "https://s1.nyt.com/du/books/images/9780805095159.jpg",
                        "book_image_width": 328,
                        "book_image_height": 495,
                        "book_review_link": "https://www.nytimes.com/2014/10/17/arts/being-mortal-by-atul-gawande.html",
                        "contributor": "by Atul Gawande",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:07",
                        "description": "The surgeon and New Yorker writer considers how doctors fail patients at the end of life, and how they can do better.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "1250076226",
                        "primary_isbn13": "9781250076229",
                        "publisher": "Metropolitan/Holt",
                        "rank": 5,
                        "rank_last_week": 0,
                        "sunday_review_link": "https://www.nytimes.com/2014/11/09/books/review/atul-gawande-being-mortal-review.html",
                        "title": "BEING MORTAL",
                        "updated_date": "2017-09-16 22:00:07",
                        "weeks_on_list": 64,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "http://www.amazon.com/Being-Mortal-Medicine-What-Matters/dp/0805095152?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9781250076229?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781250076229"
                            }
                        ]
                    }
                ]
            },
            {
                "list_id": 1,
                "list_name": "Hardcover Fiction",
                "list_name_encoded": "hardcover-fiction",
                "display_name": "Hardcover Fiction",
                "updated": "WEEKLY",
                "list_image": "https://s1.nyt.com/du/books/images/9780735225114.jpg",
                "list_image_width": 328,
                "list_image_height": 495,
                "books": [
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/Legacy-Spies-Novel-John-Carré/dp/0735225117?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "John le Carré",
                        "book_image": "https://s1.nyt.com/du/books/images/9780735225114.jpg",
                        "book_image_width": 328,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by John le Carré",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:02",
                        "description": "Peter Guillam, formerly of the British Secret Service, is pulled out of retirement to defend intelligence operations during the cold war that resulted in the deaths of people close to him.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "0735225117",
                        "primary_isbn13": "9780735225114",
                        "publisher": "Viking",
                        "rank": 1,
                        "rank_last_week": 0,
                        "sunday_review_link": "",
                        "title": "A LEGACY OF SPIES",
                        "updated_date": "2017-09-16 22:00:03",
                        "weeks_on_list": 1,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/Legacy-Spies-Novel-John-Carré/dp/0735225117?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780735225114?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780735225114"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/Secrets-Death-Dallas-Novel-Book/dp/1250123151?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "JD Robb",
                        "book_image": "https://s1.nyt.com/du/books/images/9781250123152.jpg",
                        "book_image_width": 326,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by J.D. Robb",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:02",
                        "description": "Lt. Eve Dallas investigates the murder of a professional gossip who dabbled in blackmail; by Nora Roberts, writing pseudonymously.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "1250123151",
                        "primary_isbn13": "9781250123152",
                        "publisher": "St. Martin's",
                        "rank": 2,
                        "rank_last_week": 0,
                        "sunday_review_link": "",
                        "title": "SECRETS IN DEATH",
                        "updated_date": "2017-09-16 22:00:03",
                        "weeks_on_list": 1,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/Secrets-Death-Dallas-Novel-Book/dp/1250123151?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9781250123152?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781250123152"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/Enemy-State-Mitch-Rapp-Novel-ebook/dp/B06ZZBDNYP?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Kyle Mills",
                        "book_image": "https://s1.nyt.com/du/books/images/9781476783543.jpg",
                        "book_image_width": 322,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by Kyle Mills",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:02",
                        "description": "Vince Flynn's character Mitch Rapp leaves the C.I.A. to go on a manhunt when the nephew of a Saudi King finances a terrorist group.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "1476783519",
                        "primary_isbn13": "9781476783512",
                        "publisher": "Emily Bestler/Atria",
                        "rank": 3,
                        "rank_last_week": 0,
                        "sunday_review_link": "",
                        "title": "ENEMY OF THE STATE",
                        "updated_date": "2017-09-16 22:00:03",
                        "weeks_on_list": 1,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/Enemy-State-Mitch-Rapp-Novel-ebook/dp/B06ZZBDNYP?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9781476783512?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781476783512"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/Y-Yesterday-Kinsey-Millhone-Novel-ebook/dp/B01MUBJLNC?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Sue Grafton",
                        "book_image": "https://s1.nyt.com/du/books/images/9781101614358.jpg",
                        "book_image_width": 328,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by Sue Grafton",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:02",
                        "description": "The 25th Kinsey Millhone mystery novel. A former student from an elite private school is released from prison and a sociopath returns to haunt the detective.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "0399163859",
                        "primary_isbn13": "9780399163852",
                        "publisher": "Marian Wood/Putnam",
                        "rank": 4,
                        "rank_last_week": 2,
                        "sunday_review_link": "",
                        "title": "Y IS FOR YESTERDAY",
                        "updated_date": "2017-09-16 22:00:03",
                        "weeks_on_list": 3,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/Y-Yesterday-Kinsey-Millhone-Novel-ebook/dp/B01MUBJLNC?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780399163852?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780399163852"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/Glass-Houses-Novel-Inspector-Gamache-ebook/dp/B01N9ZULCJ?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Louise Penny",
                        "book_image": "https://s1.nyt.com/du/books/images/9781466873681.jpg",
                        "book_image_width": 330,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by Louise Penny",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:02",
                        "description": "When a body is discovered in Three Pines, Chief Superintendent Gamache regrets not acting on a hunch.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "1250066190",
                        "primary_isbn13": "9781250066190",
                        "publisher": "Minotaur",
                        "rank": 5,
                        "rank_last_week": 1,
                        "sunday_review_link": "",
                        "title": "GLASS HOUSES",
                        "updated_date": "2017-09-16 22:00:03",
                        "weeks_on_list": 2,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/Glass-Houses-Novel-Inspector-Gamache-ebook/dp/B01N9ZULCJ?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9781250066190?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781250066190"
                            }
                        ]
                    }
                ]
            },
            {
                "list_id": 2,
                "list_name": "Hardcover Nonfiction",
                "list_name_encoded": "hardcover-nonfiction",
                "display_name": "Hardcover Nonfiction",
                "updated": "WEEKLY",
                "list_image": "https://s1.nyt.com/du/books/images/9780393609394.jpg",
                "list_image_width": 305,
                "list_image_height": 495,
                "books": [
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/Astrophysics-People-Hurry-deGrasse-Tyson/dp/0393609391?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Neil deGrasse Tyson",
                        "book_image": "https://s1.nyt.com/du/books/images/9780393609394.jpg",
                        "book_image_width": 305,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by Neil deGrasse Tyson",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:05",
                        "description": "A straightforward, easy-to-understand introduction to the universe.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "0393609391",
                        "primary_isbn13": "9780393609394",
                        "publisher": "Norton",
                        "rank": 1,
                        "rank_last_week": 1,
                        "sunday_review_link": "",
                        "title": "ASTROPHYSICS FOR PEOPLE IN A HURRY",
                        "updated_date": "2017-09-16 22:00:05",
                        "weeks_on_list": 19,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/Astrophysics-People-Hurry-deGrasse-Tyson/dp/0393609391?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780393609394?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780393609394"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "http://www.amazon.com/Hillbilly-Elegy-Memoir-Family-Culture-ebook/dp/B0166ISAS8?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "JD Vance",
                        "book_image": "https://s1.nyt.com/du/books/images/9780062300560.jpg",
                        "book_image_width": 328,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by J.D. Vance",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:05",
                        "description": "A Yale Law School graduate looks at the struggles of America’s white working class through his own childhood.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "0062300547",
                        "primary_isbn13": "9780062300546",
                        "publisher": "HarperCollins",
                        "rank": 2,
                        "rank_last_week": 2,
                        "sunday_review_link": "",
                        "title": "HILLBILLY ELEGY",
                        "updated_date": "2017-09-16 22:00:05",
                        "weeks_on_list": 59,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "http://www.amazon.com/Hillbilly-Elegy-Memoir-Family-Culture-ebook/dp/B0166ISAS8?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780062300546?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780062300546"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/Fantasyland-America-Haywire-500-Year-History/dp/1400067219?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Kurt Andersen",
                        "book_image": "https://s1.nyt.com/du/books/images/9781400067213.jpg",
                        "book_image_width": 328,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by Kurt Andersen",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:05",
                        "description": "The politics and culture of 21st-century America are put in the context of five centuries of historical events and movements, including elements of conspiracy theories, crackpot ideas and hucksterism.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "1400067219",
                        "primary_isbn13": "9781400067213",
                        "publisher": "Random House",
                        "rank": 3,
                        "rank_last_week": 0,
                        "sunday_review_link": "",
                        "title": "FANTASYLAND",
                        "updated_date": "2017-09-16 22:00:05",
                        "weeks_on_list": 1,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/Fantasyland-America-Haywire-500-Year-History/dp/1400067219?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9781400067213?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781400067213"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/Al-Franken-Giant-Senate/dp/1455540412?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Al Franken",
                        "book_image": "https://s1.nyt.com/du/books/images/9781455540419.jpg",
                        "book_image_width": 327,
                        "book_image_height": 495,
                        "book_review_link": "https://www.nytimes.com/2017/05/29/books/review/al-franken-giant-of-the-senate.html",
                        "contributor": "by Al Franken",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:05",
                        "description": "A memoir by the Democratic senator from Minnesota and former “Saturday Night Live” writer.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "1455540412",
                        "primary_isbn13": "9781455540419",
                        "publisher": "Twelve",
                        "rank": 4,
                        "rank_last_week": 4,
                        "sunday_review_link": "",
                        "title": "AL FRANKEN, GIANT OF THE SENATE",
                        "updated_date": "2017-09-16 22:00:05",
                        "weeks_on_list": 15,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/Al-Franken-Giant-Senate/dp/1455540412?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9781455540419?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781455540419"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/Why-Buddhism-True-Philosophy-Enlightenment/dp/1439195455?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Robert Wright",
                        "book_image": "https://s1.nyt.com/du/books/images/9781439195451.jpg",
                        "book_image_width": 328,
                        "book_image_height": 495,
                        "book_review_link": "https://www.nytimes.com/2017/08/07/books/review/why-buddhism-is-true-science-meditation-robert-wright.html",
                        "contributor": "by Robert Wright",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:05",
                        "description": "Neuroscience and psychology findings are used to support Buddhist practice and meditation.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "1439195455",
                        "primary_isbn13": "9781439195451",
                        "publisher": "Simon & Schuster",
                        "rank": 5,
                        "rank_last_week": 3,
                        "sunday_review_link": "",
                        "title": "WHY BUDDHISM IS TRUE",
                        "updated_date": "2017-09-16 22:00:05",
                        "weeks_on_list": 5,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/Why-Buddhism-True-Philosophy-Enlightenment/dp/1439195455?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9781439195451?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781439195451"
                            }
                        ]
                    }
                ]
            },
            {
                "list_id": 17,
                "list_name": "Trade Fiction Paperback",
                "list_name_encoded": "trade-fiction-paperback",
                "display_name": "Paperback Trade Fiction",
                "updated": "WEEKLY",
                "list_image": "https://s1.nyt.com/du/books/images/9781501141232.jpg",
                "list_image_width": 325,
                "list_image_height": 495,
                "books": [
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/Stephen-King-ebook/dp/B018ER7K5I?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Stephen King",
                        "book_image": "https://s1.nyt.com/du/books/images/9781501141232.jpg",
                        "book_image_width": 325,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by Stephen King",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:04",
                        "description": "",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "1501175467",
                        "primary_isbn13": "9781501175466",
                        "publisher": "Scribner",
                        "rank": 1,
                        "rank_last_week": 4,
                        "sunday_review_link": "",
                        "title": "IT",
                        "updated_date": "2017-09-16 22:00:05",
                        "weeks_on_list": 4,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/Stephen-King-ebook/dp/B018ER7K5I?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9781501175466?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781501175466"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "http://www.amazon.com/Woman-Cabin-10-Ruth-Ware-ebook/dp/B019DKO5BM?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Ruth Ware",
                        "book_image": "https://s1.nyt.com/du/books/images/9781501132940.jpg",
                        "book_image_width": 330,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by Ruth Ware",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:04",
                        "description": "",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "1501132954",
                        "primary_isbn13": "9781501132957",
                        "publisher": "Scout",
                        "rank": 2,
                        "rank_last_week": 1,
                        "sunday_review_link": "",
                        "title": "THE WOMAN IN CABIN 10",
                        "updated_date": "2017-09-16 22:00:05",
                        "weeks_on_list": 22,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "http://www.amazon.com/Woman-Cabin-10-Ruth-Ware-ebook/dp/B019DKO5BM?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9781501132957?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781501132957"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "http://www.amazon.com/Milk-Honey-Rupi-Kaur/dp/144947425X?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Rupi Kaur",
                        "book_image": "https://s1.nyt.com/du/books/images/9781449474256.jpg",
                        "book_image_width": 319,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by Rupi Kaur",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:04",
                        "description": "",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "144947425X",
                        "primary_isbn13": "9781449474256",
                        "publisher": "Andrews McMeel",
                        "rank": 3,
                        "rank_last_week": 2,
                        "sunday_review_link": "",
                        "title": "MILK AND HONEY",
                        "updated_date": "2017-09-16 22:00:05",
                        "weeks_on_list": 74,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "http://www.amazon.com/Milk-Honey-Rupi-Kaur/dp/144947425X?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9781449474256?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781449474256"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "http://www.amazon.com/Lilac-Girls-Martha-Hall-Kelly/dp/1101883073?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Martha Hall Kelly",
                        "book_image": "https://s1.nyt.com/du/books/images/9781101883075.jpg",
                        "book_image_width": 330,
                        "book_image_height": 493,
                        "book_review_link": "",
                        "contributor": "by Martha Hall Kelly",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:04",
                        "description": "",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "1101883081",
                        "primary_isbn13": "9781101883082",
                        "publisher": "Ballantine",
                        "rank": 4,
                        "rank_last_week": 3,
                        "sunday_review_link": "",
                        "title": "LILAC GIRLS",
                        "updated_date": "2017-09-16 22:00:05",
                        "weeks_on_list": 28,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "http://www.amazon.com/Lilac-Girls-Martha-Hall-Kelly/dp/1101883073?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9781101883082?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781101883082"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "http://www.amazon.com/Couple-Next-Door-Novel/dp/0735221081?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Shari Lapena",
                        "book_image": "https://s1.nyt.com/du/books/images/9780735221086.jpg",
                        "book_image_width": 328,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by Shari Lapena",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:04",
                        "description": "",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "0735221103",
                        "primary_isbn13": "9780735221109",
                        "publisher": "Penguin",
                        "rank": 5,
                        "rank_last_week": 6,
                        "sunday_review_link": "",
                        "title": "THE COUPLE NEXT DOOR",
                        "updated_date": "2017-09-16 22:00:05",
                        "weeks_on_list": 15,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "http://www.amazon.com/Couple-Next-Door-Novel/dp/0735221081?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780735221109?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780735221109"
                            }
                        ]
                    }
                ]
            },
            {
                "list_id": 4,
                "list_name": "Paperback Nonfiction",
                "list_name_encoded": "paperback-nonfiction",
                "display_name": "Paperback Nonfiction",
                "updated": "WEEKLY",
                "list_image": "https://s1.nyt.com/du/books/images/9780743247542.jpg",
                "list_image_width": 313,
                "list_image_height": 495,
                "books": [
                    {
                        "age_group": "",
                        "amazon_product_url": "http://www.amazon.com/The-Glass-Castle-A-Memoir/dp/074324754X?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Jeannette Walls",
                        "book_image": "https://s1.nyt.com/du/books/images/9780743247542.jpg",
                        "book_image_width": 313,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by Jeannette Walls",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:05",
                        "description": "",
                        "first_chapter_link": "https://www.nytimes.com/2005/03/13/books/chapters/0313-1st-walls.html",
                        "price": 0,
                        "primary_isbn10": "074324754X",
                        "primary_isbn13": "9780743247542",
                        "publisher": "Scribner",
                        "rank": 1,
                        "rank_last_week": 1,
                        "sunday_review_link": "https://www.nytimes.com/2005/03/13/books/review/013COVERPROSE.html",
                        "title": "THE GLASS CASTLE",
                        "updated_date": "2017-09-16 22:00:06",
                        "weeks_on_list": 390,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "http://www.amazon.com/The-Glass-Castle-A-Memoir/dp/074324754X?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780743247542?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780743247542"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "http://www.amazon.com/Being-Mortal-Medicine-What-Matters/dp/0805095152?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Atul Gawande",
                        "book_image": "https://s1.nyt.com/du/books/images/9780805095159.jpg",
                        "book_image_width": 328,
                        "book_image_height": 495,
                        "book_review_link": "https://www.nytimes.com/2014/10/17/arts/being-mortal-by-atul-gawande.html",
                        "contributor": "by Atul Gawande",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:05",
                        "description": "",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "1250076226",
                        "primary_isbn13": "9781250076229",
                        "publisher": "Picador",
                        "rank": 2,
                        "rank_last_week": 0,
                        "sunday_review_link": "https://www.nytimes.com/2014/11/09/books/review/atul-gawande-being-mortal-review.html",
                        "title": "BEING MORTAL",
                        "updated_date": "2017-09-16 22:00:06",
                        "weeks_on_list": 1,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "http://www.amazon.com/Being-Mortal-Medicine-What-Matters/dp/0805095152?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9781250076229?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781250076229"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/Lost-City-Monkey-God-Story-ebook/dp/B01G1K1RTA?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Douglas Preston",
                        "book_image": "https://s1.nyt.com/du/books/images/9781455540020.jpg",
                        "book_image_width": 329,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by Douglas Preston",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:05",
                        "description": "",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "isbn10 mus",
                        "primary_isbn13": "9781455540013",
                        "publisher": "Grand Central",
                        "rank": 3,
                        "rank_last_week": 0,
                        "sunday_review_link": "",
                        "title": "THE LOST CITY OF THE MONKEY GOD",
                        "updated_date": "2017-09-16 22:00:06",
                        "weeks_on_list": 1,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/Lost-City-Monkey-God-Story-ebook/dp/B01G1K1RTA?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9781455540013?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781455540013"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "http://www.amazon.com/Just-Mercy-Story-Justice-Redemption-ebook/dp/B00JYWVYLY?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Bryan Stevenson",
                        "book_image": "https://s1.nyt.com/du/books/images/9780812994520.jpg",
                        "book_image_width": 128,
                        "book_image_height": 193,
                        "book_review_link": "",
                        "contributor": "by Bryan Stevenson",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:05",
                        "description": "",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "081298496X",
                        "primary_isbn13": "9780812984965",
                        "publisher": "Spiegel & Grau",
                        "rank": 4,
                        "rank_last_week": 2,
                        "sunday_review_link": "https://www.nytimes.com/2014/10/19/books/review/just-mercy-by-bryan-stevenson.html",
                        "title": "JUST MERCY",
                        "updated_date": "2017-09-16 22:00:06",
                        "weeks_on_list": 77,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "http://www.amazon.com/Just-Mercy-Story-Justice-Redemption-ebook/dp/B00JYWVYLY?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780812984965?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780812984965"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/Tyranny-Twenty-Lessons-Twentieth-Century/dp/0804190119?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Timothy Snyder",
                        "book_image": "https://s1.nyt.com/du/books/images/9780804190114.jpg",
                        "book_image_width": 330,
                        "book_image_height": 465,
                        "book_review_link": "",
                        "contributor": "by Timothy Snyder",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:05",
                        "description": "",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "0804190119",
                        "primary_isbn13": "9780804190114",
                        "publisher": "Tim Duggan",
                        "rank": 5,
                        "rank_last_week": 5,
                        "sunday_review_link": "",
                        "title": "ON TYRANNY",
                        "updated_date": "2017-09-16 22:00:06",
                        "weeks_on_list": 28,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/Tyranny-Twenty-Lessons-Twentieth-Century/dp/0804190119?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780804190114?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780804190114"
                            }
                        ]
                    }
                ]
            },
            {
                "list_id": 24,
                "list_name": "Advice How-To and Miscellaneous",
                "list_name_encoded": "advice-how-to-and-miscellaneous",
                "display_name": "Advice, How-To & Miscellaneous",
                "updated": "WEEKLY",
                "list_image": "https://s1.nyt.com/du/books/images/9780062457714.jpg",
                "list_image_width": 330,
                "list_image_height": 495,
                "books": [
                    {
                        "age_group": "",
                        "amazon_product_url": "http://www.amazon.com/Subtle-Art-Not-Giving-Counterintuitive/dp/0062457713?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Mark Manson",
                        "book_image": "https://s1.nyt.com/du/books/images/9780062457714.jpg",
                        "book_image_width": 330,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by Mark Manson",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:05",
                        "description": "",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "0062457713",
                        "primary_isbn13": "9780062457714",
                        "publisher": "HarperOne/HarperCollins",
                        "rank": 1,
                        "rank_last_week": 1,
                        "sunday_review_link": "",
                        "title": "THE SUBTLE ART OF NOT GIVING A ----------",
                        "updated_date": "2017-09-16 22:00:05",
                        "weeks_on_list": 39,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "http://www.amazon.com/Subtle-Art-Not-Giving-Counterintuitive/dp/0062457713?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780062457714?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780062457714"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "http://www.amazon.com/You-Are-Badass-Doubting-Greatness-ebook/dp/B00B3M3VWS?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Jen Sincero",
                        "book_image": "https://s1.nyt.com/du/books/images/9780762447695.jpg",
                        "book_image_width": 315,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by Jen Sincero",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:05",
                        "description": "",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "0762447699",
                        "primary_isbn13": "9780762447695",
                        "publisher": "Running Press",
                        "rank": 2,
                        "rank_last_week": 3,
                        "sunday_review_link": "",
                        "title": "YOU ARE A BADASS",
                        "updated_date": "2017-09-16 22:00:05",
                        "weeks_on_list": 87,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "http://www.amazon.com/You-Are-Badass-Doubting-Greatness-ebook/dp/B00B3M3VWS?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780762447695?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780762447695"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/End-Alzheimers-Program-Prevent-Cognitive/dp/0735216207?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Dale Bredesen",
                        "book_image": "https://s1.nyt.com/du/books/images/9780735216204.jpg",
                        "book_image_width": 328,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by Dale Bredesen",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:05",
                        "description": "",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "0735216207",
                        "primary_isbn13": "9780735216204",
                        "publisher": "Avery",
                        "rank": 3,
                        "rank_last_week": 4,
                        "sunday_review_link": "",
                        "title": "THE END OF ALZHEIMER'S",
                        "updated_date": "2017-09-16 22:00:05",
                        "weeks_on_list": 3,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/End-Alzheimers-Program-Prevent-Cognitive/dp/0735216207?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780735216204?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780735216204"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/Make-Your-Bed-Little-Things/dp/1455570249?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "William H McRaven",
                        "book_image": "https://s1.nyt.com/du/books/images/9781455570249.jpg",
                        "book_image_width": 330,
                        "book_image_height": 466,
                        "book_review_link": "",
                        "contributor": "by William H. McRaven",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:05",
                        "description": "",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "1455570249",
                        "primary_isbn13": "9781455570249",
                        "publisher": "Grand Central",
                        "rank": 4,
                        "rank_last_week": 2,
                        "sunday_review_link": "",
                        "title": "MAKE YOUR BED",
                        "updated_date": "2017-09-16 22:00:05",
                        "weeks_on_list": 23,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/Make-Your-Bed-Little-Things/dp/1455570249?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9781455570249?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781455570249"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "http://www.amazon.com/The-Love-Languages-Secret-Lasts/dp/0802473156?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Gary Chapman",
                        "book_image": "https://s1.nyt.com/du/books/images/9780802473158.jpg",
                        "book_image_width": 268,
                        "book_image_height": 400,
                        "book_review_link": "",
                        "contributor": "by Gary Chapman",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:05",
                        "description": "",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "080241270X",
                        "primary_isbn13": "9780802412706",
                        "publisher": "Northfield",
                        "rank": 5,
                        "rank_last_week": 5,
                        "sunday_review_link": "",
                        "title": "THE FIVE LOVE LANGUAGES",
                        "updated_date": "2017-09-16 22:00:05",
                        "weeks_on_list": 217,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "http://www.amazon.com/The-Love-Languages-Secret-Lasts/dp/0802473156?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780802412706?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780802412706"
                            }
                        ]
                    }
                ]
            },
            {
                "list_id": 13,
                "list_name": "Childrens Middle Grade Hardcover",
                "list_name_encoded": "childrens-middle-grade-hardcover",
                "display_name": "Children’s Middle Grade Hardcover",
                "updated": "WEEKLY",
                "list_image": "https://s1.nyt.com/du/books/images/9780375899881.jpg",
                "list_image_width": 329,
                "list_image_height": 495,
                "books": [
                    {
                        "age_group": "",
                        "amazon_product_url": "http://www.amazon.com/Wonder-R-J-Palacio-ebook/dp/B0051ANPZQ?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "R J Palacio",
                        "book_image": "https://s1.nyt.com/du/books/images/9780375899881.jpg",
                        "book_image_width": 329,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by R. J. Palacio",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:04",
                        "description": "A boy with a facial deformity starts school.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "0375869026",
                        "primary_isbn13": "9780375869020",
                        "publisher": "Knopf",
                        "rank": 1,
                        "rank_last_week": 1,
                        "sunday_review_link": "https://www.nytimes.com/2012/04/08/books/review/wonder-by-r-j-palacio.html",
                        "title": "WONDER",
                        "updated_date": "2017-09-16 22:00:04",
                        "weeks_on_list": 109,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "http://www.amazon.com/Wonder-R-J-Palacio-ebook/dp/B0051ANPZQ?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780375869020?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780375869020"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/Minecraft-Island-Novel-Max-Brooks/dp/0399181776?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Max Brooks",
                        "book_image": "https://s1.nyt.com/du/books/images/9780399181771.jpg",
                        "book_image_width": 328,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by Max Brooks",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:04",
                        "description": "A lone castaway faces dangers in a strange new world.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "0399181776",
                        "primary_isbn13": "9780399181771",
                        "publisher": "Del Rey",
                        "rank": 2,
                        "rank_last_week": 2,
                        "sunday_review_link": "",
                        "title": "MINECRAFT: THE ISLAND",
                        "updated_date": "2017-09-16 22:00:04",
                        "weeks_on_list": 8,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/Minecraft-Island-Novel-Max-Brooks/dp/0399181776?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780399181771?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780399181771"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/Good-Night-Stories-Rebel-Girls/dp/014198600X?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Elena Favilli and Francesca Cavallo",
                        "book_image": "https://s1.nyt.com/du/books/images/9780997895810.jpg",
                        "book_image_width": 330,
                        "book_image_height": 456,
                        "book_review_link": "",
                        "contributor": "by Elena Favilli and Francesca Cavallo",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:04",
                        "description": "Fairy tale versions of the lives of 100 influential women.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "0997895810",
                        "primary_isbn13": "9780997895810",
                        "publisher": "Timbuktu Labs",
                        "rank": 3,
                        "rank_last_week": 3,
                        "sunday_review_link": "",
                        "title": "GOOD NIGHT STORIES FOR REBEL GIRLS",
                        "updated_date": "2017-09-16 22:00:04",
                        "weeks_on_list": 11,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/Good-Night-Stories-Rebel-Girls/dp/014198600X?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780997895810?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780997895810"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/Trials-Apollo-Book-Dark-Prophecy/dp/1484746422?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Rick Riordan",
                        "book_image": "https://s1.nyt.com/du/books/images/9781484746424.jpg",
                        "book_image_width": 325,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by Rick Riordan",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:04",
                        "description": "Lester, a.k.a. Apollo, summons the help of demigods to restore an Oracle.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "1484746422",
                        "primary_isbn13": "9781484746424",
                        "publisher": "Disney-Hyperion",
                        "rank": 4,
                        "rank_last_week": 5,
                        "sunday_review_link": "",
                        "title": "THE DARK PROPHECY",
                        "updated_date": "2017-09-16 22:00:04",
                        "weeks_on_list": 19,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/Trials-Apollo-Book-Dark-Prophecy/dp/1484746422?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9781484746424?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781484746424"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/Laugh-Out-Loud-James-Patterson/dp/031643146X?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "James Patterson and Chris Grabenstein",
                        "book_image": "https://s1.nyt.com/du/books/images/9780316431460.jpg",
                        "book_image_width": 330,
                        "book_image_height": 490,
                        "book_review_link": "",
                        "contributor": "by James Patterson and Chris Grabenstein. Illustrated by Jeff Ebbeler",
                        "contributor_note": "Illustrated by Jeff Ebbeler",
                        "created_date": "2017-09-14 16:00:04",
                        "description": "Jimmy dreams of starting a book company that is for kids and run by kids.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "031643146X",
                        "primary_isbn13": "9780316431460",
                        "publisher": "Jimmy Patterson",
                        "rank": 5,
                        "rank_last_week": 4,
                        "sunday_review_link": "",
                        "title": "LAUGH OUT LOUD",
                        "updated_date": "2017-09-16 22:00:04",
                        "weeks_on_list": 2,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/Laugh-Out-Loud-James-Patterson/dp/031643146X?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780316431460?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780316431460"
                            }
                        ]
                    }
                ]
            },
            {
                "list_id": 7,
                "list_name": "Picture Books",
                "list_name_encoded": "picture-books",
                "display_name": "Children’s Picture Books",
                "updated": "WEEKLY",
                "list_image": "https://s1.nyt.com/du/books/images/9781943200009.jpg",
                "list_image_width": 330,
                "list_image_height": 394,
                "books": [
                    {
                        "age_group": "",
                        "amazon_product_url": "http://www.amazon.com/What-Do-You-Problem/dp/1943200009?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Kobi Yamada",
                        "book_image": "https://s1.nyt.com/du/books/images/9781943200009.jpg",
                        "book_image_width": 330,
                        "book_image_height": 394,
                        "book_review_link": "",
                        "contributor": "by Kobi Yamada. Illustrated by Mae Besom",
                        "contributor_note": "Illustrated by Mae Besom",
                        "created_date": "2017-09-14 16:00:06",
                        "description": "Problems can be lessons in disguise.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "1943200009",
                        "primary_isbn13": "9781943200009",
                        "publisher": "Compendium",
                        "rank": 1,
                        "rank_last_week": 1,
                        "sunday_review_link": "",
                        "title": "WHAT DO YOU DO WITH A PROBLEM?",
                        "updated_date": "2017-09-16 22:00:06",
                        "weeks_on_list": 19,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "http://www.amazon.com/What-Do-You-Problem/dp/1943200009?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9781943200009?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781943200009"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/She-Persisted-American-Women-Changed/dp/1524741728?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Chelsea Clinton",
                        "book_image": "https://s1.nyt.com/du/books/images/9781524741723.jpg",
                        "book_image_width": 330,
                        "book_image_height": 401,
                        "book_review_link": "",
                        "contributor": "by Chelsea Clinton. Illustrated by Alexandra Boiger",
                        "contributor_note": "Illustrated by Alexandra Boiger",
                        "created_date": "2017-09-14 16:00:06",
                        "description": "Bringing to life 13 American women who changed the world.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "1524741728",
                        "primary_isbn13": "9781524741723",
                        "publisher": "Philomel",
                        "rank": 2,
                        "rank_last_week": 2,
                        "sunday_review_link": "",
                        "title": "SHE PERSISTED",
                        "updated_date": "2017-09-16 22:00:06",
                        "weeks_on_list": 15,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/She-Persisted-American-Women-Changed/dp/1524741728?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9781524741723?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781524741723"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "http://www.amazon.com/Dragons-Love-Tacos-Adam-Rubin/dp/0803736800?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Adam Rubin",
                        "book_image": "https://s1.nyt.com/du/books/images/9780803736801.jpg",
                        "book_image_width": 330,
                        "book_image_height": 330,
                        "book_review_link": "",
                        "contributor": "by Adam Rubin. Illustrated by Daniel Salmieri",
                        "contributor_note": "Illustrated by Daniel Salmieri",
                        "created_date": "2017-09-14 16:00:06",
                        "description": "What to serve your dragon-guests.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "0803736800",
                        "primary_isbn13": "9780803736801",
                        "publisher": "Dial",
                        "rank": 3,
                        "rank_last_week": 4,
                        "sunday_review_link": "",
                        "title": "DRAGONS LOVE TACOS",
                        "updated_date": "2017-09-16 22:00:06",
                        "weeks_on_list": 186,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "http://www.amazon.com/Dragons-Love-Tacos-Adam-Rubin/dp/0803736800?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780803736801?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780803736801"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "http://www.amazon.com/What-Do-You-Idea/dp/1938298071?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Kobi Yamada",
                        "book_image": "https://s1.nyt.com/du/books/images/9781938298073.jpg",
                        "book_image_width": 330,
                        "book_image_height": 393,
                        "book_review_link": "",
                        "contributor": "by Kobi Yamada. Illustrated by Mae Besom",
                        "contributor_note": "Illustrated by Mae Besom",
                        "created_date": "2017-09-14 16:00:06",
                        "description": "Giving a new idea the room to grow.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "1938298071",
                        "primary_isbn13": "9781938298073",
                        "publisher": "Compendium",
                        "rank": 4,
                        "rank_last_week": 3,
                        "sunday_review_link": "",
                        "title": "WHAT DO YOU DO WITH AN IDEA?",
                        "updated_date": "2017-09-16 22:00:06",
                        "weeks_on_list": 36,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "http://www.amazon.com/What-Do-You-Idea/dp/1938298071?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9781938298073?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781938298073"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "http://www.amazon.com/The-Wonderful-Things-You-Will/dp/0385376715?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Emily Winfield Martin",
                        "book_image": "https://s1.nyt.com/du/books/images/9780385376716.jpg",
                        "book_image_width": 330,
                        "book_image_height": 347,
                        "book_review_link": "",
                        "contributor": "by Emily Winfield Martin",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:06",
                        "description": "A celebration of future possibilities.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "0385376715",
                        "primary_isbn13": "9780385376716",
                        "publisher": "Random House",
                        "rank": 5,
                        "rank_last_week": 5,
                        "sunday_review_link": "",
                        "title": "THE WONDERFUL THINGS YOU WILL BE",
                        "updated_date": "2017-09-16 22:00:06",
                        "weeks_on_list": 102,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "http://www.amazon.com/The-Wonderful-Things-You-Will/dp/0385376715?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780385376716?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780385376716"
                            }
                        ]
                    }
                ]
            },
            {
                "list_id": 10,
                "list_name": "Series Books",
                "list_name_encoded": "series-books",
                "display_name": "Children’s Series",
                "updated": "WEEKLY",
                "list_image": "https://s1.nyt.com/du/books/images/9780545935210.jpg",
                "list_image_width": 330,
                "list_image_height": 488,
                "books": [
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/Dog-Man-Kitties-Creator-Underpants/dp/0545935210?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Dav Pilkey",
                        "book_image": "https://s1.nyt.com/du/books/images/9780545935210.jpg",
                        "book_image_width": 330,
                        "book_image_height": 488,
                        "book_review_link": "",
                        "contributor": "by Dav Pilkey",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:03",
                        "description": "A dog’s head is combined with a policeman’s body to create this hybrid supercop hound.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "0545935210",
                        "primary_isbn13": "9780545935210",
                        "publisher": "Scholastic",
                        "rank": 1,
                        "rank_last_week": 1,
                        "sunday_review_link": "",
                        "title": "DOG MAN",
                        "updated_date": "2017-09-16 22:00:04",
                        "weeks_on_list": 2,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/Dog-Man-Kitties-Creator-Underpants/dp/0545935210?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780545935210?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780545935210"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "http://www.amazon.com/Heir-Fire-Throne-Glass-Book-ebook/dp/B00I43Z1J0?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Sarah J Maas",
                        "book_image": "https://s1.nyt.com/du/books/images/9781619630666.jpg",
                        "book_image_width": 315,
                        "book_image_height": 475,
                        "book_review_link": "",
                        "contributor": "by Sarah J. Maas",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:03",
                        "description": "Celaena must battle evil forces threatening her realm.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "None",
                        "primary_isbn13": "9781681195773",
                        "publisher": "Bloomsbury",
                        "rank": 2,
                        "rank_last_week": 0,
                        "sunday_review_link": "",
                        "title": "THRONE OF GLASS",
                        "updated_date": "2017-09-16 22:00:04",
                        "weeks_on_list": 16,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "http://www.amazon.com/Heir-Fire-Throne-Glass-Book-ebook/dp/B00I43Z1J0?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9781681195773?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781681195773"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "http://www.amazon.com/Harry-Potter-And-Order-Phoenix/dp/0439358078?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "J K Rowling",
                        "book_image": "https://s1.nyt.com/du/books/images/9780590353427.jpg",
                        "book_image_width": 328,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by J. K. Rowling",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:03",
                        "description": "A wizard hones his conjuring skills in the service of fighting evil.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "133821666X",
                        "primary_isbn13": "9781338216660",
                        "publisher": "Scholastic",
                        "rank": 3,
                        "rank_last_week": 2,
                        "sunday_review_link": "",
                        "title": "HARRY POTTER",
                        "updated_date": "2017-09-16 22:00:04",
                        "weeks_on_list": 442,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "http://www.amazon.com/Harry-Potter-And-Order-Phoenix/dp/0439358078?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9781338216660?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781338216660"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "http://www.amazon.com/Diary-Wimpy-Kid-Hard-Luck/dp/1419711326?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Jeff Kinney",
                        "book_image": "https://s1.nyt.com/du/books/images/9781419711329.jpg",
                        "book_image_width": 330,
                        "book_image_height": 484,
                        "book_review_link": "",
                        "contributor": "written and illustrated by Jeff Kinney",
                        "contributor_note": "written and illustrated by Jeff Kinney",
                        "created_date": "2017-09-14 16:00:03",
                        "description": "The travails and challenges of adolescence.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "1419723448",
                        "primary_isbn13": "9781419723445",
                        "publisher": "Amulet",
                        "rank": 4,
                        "rank_last_week": 3,
                        "sunday_review_link": "",
                        "title": "DIARY OF A WIMPY KID",
                        "updated_date": "2017-09-16 22:00:04",
                        "weeks_on_list": 443,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "http://www.amazon.com/Diary-Wimpy-Kid-Hard-Luck/dp/1419711326?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9781419723445?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781419723445"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "http://www.amazon.com/Captain-Underpants-Revolting-Radioactive-Robo-Boxers/dp/0545175364?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Dav Pilkey",
                        "book_image": "https://s1.nyt.com/du/books/images/9780545175340.jpg",
                        "book_image_width": 329,
                        "book_image_height": 475,
                        "book_review_link": "https://www.nytimes.com/2011/06/29/books/review/childrens-books-super-diaper-baby-2-by-dav-pilkey.html",
                        "contributor": "written and illustrated by Dav Pilkey",
                        "contributor_note": "written and illustrated by Dav Pilkey",
                        "created_date": "2017-09-14 16:00:03",
                        "description": "Boys and their principal fight evil.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "0590846280",
                        "primary_isbn13": "9780590846288",
                        "publisher": "Scholastic",
                        "rank": 5,
                        "rank_last_week": 6,
                        "sunday_review_link": "",
                        "title": "CAPTAIN UNDERPANTS",
                        "updated_date": "2017-09-16 22:00:04",
                        "weeks_on_list": 83,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "http://www.amazon.com/Captain-Underpants-Revolting-Radioactive-Robo-Boxers/dp/0545175364?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780590846288?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780590846288"
                            }
                        ]
                    }
                ]
            },
            {
                "list_id": 14,
                "list_name": "Young Adult Hardcover",
                "list_name_encoded": "young-adult-hardcover",
                "display_name": "Young Adult Hardcover",
                "updated": "WEEKLY",
                "list_image": "https://s1.nyt.com/du/books/images/9780062498533.jpg",
                "list_image_width": 328,
                "list_image_height": 495,
                "books": [
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/Hate-U-Give-Angie-Thomas/dp/0062498533?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Angie Thomas",
                        "book_image": "https://s1.nyt.com/du/books/images/9780062498533.jpg",
                        "book_image_width": 328,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by Angie Thomas",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:04",
                        "description": "A 16-year-old girl sees a police officer kill her friend.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "0062498533",
                        "primary_isbn13": "9780062498533",
                        "publisher": "Balzer & Bray",
                        "rank": 1,
                        "rank_last_week": 1,
                        "sunday_review_link": "",
                        "title": "THE HATE U GIVE",
                        "updated_date": "2017-09-16 22:00:05",
                        "weeks_on_list": 28,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/Hate-U-Give-Angie-Thomas/dp/0062498533?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780062498533?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780062498533"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/Wonder-Woman-Warbringer-DC-Icons/dp/0399549730?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Leigh Bardugo",
                        "book_image": "https://s1.nyt.com/du/books/images/9780399549731.jpg",
                        "book_image_width": 330,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by Leigh Bardugo",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:04",
                        "description": "Diana, Princess of the Amazons, saves a descendant of Helen of Troy and together they fight to save both of their worlds.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "0399549730",
                        "primary_isbn13": "9780399549731",
                        "publisher": "Random House",
                        "rank": 2,
                        "rank_last_week": 3,
                        "sunday_review_link": "",
                        "title": "WONDER WOMAN: WARBRINGER",
                        "updated_date": "2017-09-16 22:00:05",
                        "weeks_on_list": 2,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/Wonder-Woman-Warbringer-DC-Icons/dp/0399549730?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780399549731?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780399549731"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/Bernie-Sanders-Guide-Political-Revolution/dp/1250138906?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Bernie Sanders",
                        "book_image": "https://s1.nyt.com/du/books/images/9781250138903.jpg",
                        "book_image_width": 330,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by Bernie Sanders. Illustrated by Jude Buffum",
                        "contributor_note": "Illustrated by Jude Buffum",
                        "created_date": "2017-09-14 16:00:04",
                        "description": "The Vermont senator and former presidential candidate continues the fight for his agenda for political and social change.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "1250138906",
                        "primary_isbn13": "9781250138903",
                        "publisher": "Godwin/Holt",
                        "rank": 3,
                        "rank_last_week": 2,
                        "sunday_review_link": "",
                        "title": "BERNIE SANDERS GUIDE TO POLITICAL REVOLUTION",
                        "updated_date": "2017-09-16 22:00:05",
                        "weeks_on_list": 2,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/Bernie-Sanders-Guide-Political-Revolution/dp/1250138906?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9781250138903?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781250138903"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/One-Us-Lying-Karen-McManus/dp/1524714682?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Karen M McManus",
                        "book_image": "https://s1.nyt.com/du/books/images/9781524714680.jpg",
                        "book_image_width": 327,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by Karen M. McManus",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:04",
                        "description": "For five students, a detour into detention ends in murder.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "1524714682",
                        "primary_isbn13": "9781524714680",
                        "publisher": "Delacorte",
                        "rank": 4,
                        "rank_last_week": 4,
                        "sunday_review_link": "",
                        "title": "ONE OF US IS LYING",
                        "updated_date": "2017-09-16 22:00:05",
                        "weeks_on_list": 12,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/One-Us-Lying-Karen-McManus/dp/1524714682?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9781524714680?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781524714680"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/They-Both-Die-at-End/dp/0062457799?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Adam Silvera",
                        "book_image": "https://s1.nyt.com/du/books/images/9780062457790.jpg",
                        "book_image_width": 327,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by Adam Silvera",
                        "contributor_note": "",
                        "created_date": "2017-09-14 16:00:04",
                        "description": "Through an app, two young people meet up on the last day of their lives.",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "0062457799",
                        "primary_isbn13": "9780062457790",
                        "publisher": "HarperTeen",
                        "rank": 5,
                        "rank_last_week": 0,
                        "sunday_review_link": "",
                        "title": "THEY BOTH DIE AT THE END",
                        "updated_date": "2017-09-16 22:00:05",
                        "weeks_on_list": 1,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/They-Both-Die-at-End/dp/0062457799?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780062457790?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780062457790"
                            }
                        ]
                    }
                ]
            },
            {
                "list_id": 532,
                "list_name": "Business Books",
                "list_name_encoded": "business-books",
                "display_name": "Business",
                "updated": "MONTHLY",
                "list_image": "https://s1.nyt.com/du/books/images/9781501111129.jpg",
                "list_image_width": 328,
                "list_image_height": 495,
                "books": [
                    {
                        "age_group": "",
                        "amazon_product_url": "http://www.amazon.com/Grit-The-Power-Passion-Perseverance-ebook/dp/B010MH9V3W?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Angela Duckworth",
                        "book_image": "https://s1.nyt.com/du/books/images/9781501111129.jpg",
                        "book_image_width": 328,
                        "book_image_height": 495,
                        "book_review_link": "https://www.nytimes.com/2016/05/08/books/review/grit-by-angela-duckworth.html",
                        "contributor": "by Angela Duckworth",
                        "contributor_note": "",
                        "created_date": "2017-09-08 15:00:03",
                        "description": "",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "1501111108",
                        "primary_isbn13": "9781501111105",
                        "publisher": "Scribner",
                        "rank": 1,
                        "rank_last_week": 0,
                        "sunday_review_link": "",
                        "title": "GRIT",
                        "updated_date": "2017-09-08 15:00:03",
                        "weeks_on_list": 0,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "http://www.amazon.com/Grit-The-Power-Passion-Perseverance-ebook/dp/B010MH9V3W?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9781501111105?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781501111105"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "http://www.amazon.com/Outliers-Story-Success-Malcolm-Gladwell/dp/0316017930?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Malcolm Gladwell",
                        "book_image": "https://s1.nyt.com/du/books/images/9780316017930.jpg",
                        "book_image_width": 184,
                        "book_image_height": 280,
                        "book_review_link": "https://www.nytimes.com/2008/11/18/books/18kaku.html",
                        "contributor": "by Malcolm Gladwell",
                        "contributor_note": "",
                        "created_date": "2017-09-08 15:00:03",
                        "description": "",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "0316017930",
                        "primary_isbn13": "9780316017930",
                        "publisher": "Back Bay/Little, Brown",
                        "rank": 2,
                        "rank_last_week": 0,
                        "sunday_review_link": "https://www.nytimes.com/2008/11/30/books/review/Leonhardt-t.html",
                        "title": "OUTLIERS",
                        "updated_date": "2017-09-08 15:00:03",
                        "weeks_on_list": 0,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "http://www.amazon.com/Outliers-Story-Success-Malcolm-Gladwell/dp/0316017930?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780316017930?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780316017930"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "http://www.amazon.com/Red-Notice-Finance-Murder-Justice/dp/147675571X?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Bill Browder",
                        "book_image": "https://s1.nyt.com/du/books/images/9781476755717.jpg",
                        "book_image_width": 328,
                        "book_image_height": 495,
                        "book_review_link": "https://www.nytimes.com/2015/02/02/arts/bill-browders-red-notice-about-his-russian-misadventures.html",
                        "contributor": "by Bill Browder",
                        "contributor_note": "",
                        "created_date": "2017-09-08 15:00:03",
                        "description": "",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "1476755744",
                        "primary_isbn13": "9781476755748",
                        "publisher": "Simon & Schuster",
                        "rank": 3,
                        "rank_last_week": 0,
                        "sunday_review_link": "https://www.nytimes.com/2015/03/22/books/review/bill-browders-red-notice.html",
                        "title": "RED NOTICE",
                        "updated_date": "2017-09-08 15:00:03",
                        "weeks_on_list": 0,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "http://www.amazon.com/Red-Notice-Finance-Murder-Justice/dp/147675571X?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9781476755748?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781476755748"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/Emigrant-Edge-How-Make-America/dp/1501169270?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Brian Buffini",
                        "book_image": "https://s1.nyt.com/du/books/images/9781501169274.jpg",
                        "book_image_width": 319,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by Brian Buffini",
                        "contributor_note": "",
                        "created_date": "2017-09-08 15:00:03",
                        "description": "",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "1501169270",
                        "primary_isbn13": "9781501169274",
                        "publisher": "Howard Books",
                        "rank": 4,
                        "rank_last_week": 0,
                        "sunday_review_link": "",
                        "title": "EMIGRANT EDGE",
                        "updated_date": "2017-09-08 15:00:03",
                        "weeks_on_list": 0,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/Emigrant-Edge-How-Make-America/dp/1501169270?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9781501169274?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781501169274"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "http://www.amazon.com/Thinking-Fast-Slow-Daniel-Kahneman-ebook/dp/B00555X8OA?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Daniel Kahneman",
                        "book_image": "https://s1.nyt.com/du/books/images/9781429969352.jpg",
                        "book_image_width": 330,
                        "book_image_height": 490,
                        "book_review_link": "https://www.nytimes.com/2011/11/27/books/review/thinking-fast-and-slow-by-daniel-kahneman-book-review.html",
                        "contributor": "by Daniel Kahneman",
                        "contributor_note": "",
                        "created_date": "2017-09-08 15:00:03",
                        "description": "",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "0374533555",
                        "primary_isbn13": "9780374533557",
                        "publisher": "Farrar, Straus & Giroux",
                        "rank": 5,
                        "rank_last_week": 0,
                        "sunday_review_link": "https://www.nytimes.com/2011/11/27/books/review/thinking-fast-and-slow-by-daniel-kahneman-book-review.html",
                        "title": "THINKING, FAST AND SLOW",
                        "updated_date": "2017-09-08 15:00:03",
                        "weeks_on_list": 0,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "http://www.amazon.com/Thinking-Fast-Slow-Daniel-Kahneman-ebook/dp/B00555X8OA?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780374533557?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780374533557"
                            }
                        ]
                    }
                ]
            },
            {
                "list_id": 531,
                "list_name": "Science",
                "list_name_encoded": "science",
                "display_name": "Science",
                "updated": "MONTHLY",
                "list_image": "https://s1.nyt.com/du/books/images/9780393609394.jpg",
                "list_image_width": 305,
                "list_image_height": 495,
                "books": [
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/Astrophysics-People-Hurry-deGrasse-Tyson/dp/0393609391?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Neil deGrasse Tyson",
                        "book_image": "https://s1.nyt.com/du/books/images/9780393609394.jpg",
                        "book_image_width": 305,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by Neil deGrasse Tyson",
                        "contributor_note": "",
                        "created_date": "2017-09-08 15:00:04",
                        "description": "",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "0393609391",
                        "primary_isbn13": "9780393609394",
                        "publisher": "Norton",
                        "rank": 1,
                        "rank_last_week": 1,
                        "sunday_review_link": "",
                        "title": "ASTROPHYSICS FOR PEOPLE IN A HURRY",
                        "updated_date": "2017-09-08 15:00:04",
                        "weeks_on_list": 0,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/Astrophysics-People-Hurry-deGrasse-Tyson/dp/0393609391?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780393609394?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780393609394"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "http://www.amazon.com/Sapiens-A-Brief-History-Humankind-ebook/dp/B00ICN066A?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Yuval Noah Harari",
                        "book_image": "https://s1.nyt.com/du/books/images/9780062316097.jpg",
                        "book_image_width": 128,
                        "book_image_height": 192,
                        "book_review_link": "",
                        "contributor": "by Yuval Noah Harari",
                        "contributor_note": "",
                        "created_date": "2017-09-08 15:00:04",
                        "description": "",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "0062316095",
                        "primary_isbn13": "9780062316097",
                        "publisher": "Harper",
                        "rank": 2,
                        "rank_last_week": 2,
                        "sunday_review_link": "",
                        "title": "SAPIENS",
                        "updated_date": "2017-09-08 15:00:04",
                        "weeks_on_list": 0,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "http://www.amazon.com/Sapiens-A-Brief-History-Humankind-ebook/dp/B00ICN066A?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780062316097?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780062316097"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/End-Alzheimers-Program-Prevent-Cognitive/dp/0735216207?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Dale Bredesen",
                        "book_image": "https://s1.nyt.com/du/books/images/9780735216204.jpg",
                        "book_image_width": 328,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by Dale Bredesen",
                        "contributor_note": "",
                        "created_date": "2017-09-08 15:00:04",
                        "description": "",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "0735216207",
                        "primary_isbn13": "9780735216204",
                        "publisher": "Avery",
                        "rank": 3,
                        "rank_last_week": 0,
                        "sunday_review_link": "",
                        "title": "THE END OF ALZHEIMER'S",
                        "updated_date": "2017-09-08 15:00:04",
                        "weeks_on_list": 0,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/End-Alzheimers-Program-Prevent-Cognitive/dp/0735216207?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780735216204?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780735216204"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "http://www.amazon.com/The-Immortal-Life-Henrietta-Lacks/dp/1400052181?tag=NYTBS-20",
                        "article_chapter_link": "https://www.nytimes.com/2010/02/02/health/02seco.html",
                        "author": "Rebecca Skloot",
                        "book_image": "https://s1.nyt.com/du/books/images/9781400052189.jpg",
                        "book_image_width": 182,
                        "book_image_height": 280,
                        "book_review_link": "https://www.nytimes.com/2010/02/03/books/03book.html",
                        "contributor": "by Rebecca Skloot",
                        "contributor_note": "",
                        "created_date": "2017-09-08 15:00:04",
                        "description": "",
                        "first_chapter_link": "https://www.nytimes.com/2010/03/03/books/excerpt-immortal-life-of-henrietta-lacks.html",
                        "price": 0,
                        "primary_isbn10": "1400052181",
                        "primary_isbn13": "9781400052189",
                        "publisher": "Broadway",
                        "rank": 4,
                        "rank_last_week": 6,
                        "sunday_review_link": "https://www.nytimes.com/2010/02/07/books/review/Margonelli-t.html",
                        "title": "THE IMMORTAL LIFE OF HENRIETTA LACKS",
                        "updated_date": "2017-09-08 15:00:04",
                        "weeks_on_list": 0,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "http://www.amazon.com/The-Immortal-Life-Henrietta-Lacks/dp/1400052181?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9781400052189?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781400052189"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "http://www.amazon.com/When-Breath-Becomes-Paul-Kalanithi-ebook/dp/B00XSSYR50?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Paul Kalanithi",
                        "book_image": "https://s1.nyt.com/du/books/images/9780812988413.jpg",
                        "book_image_width": 330,
                        "book_image_height": 488,
                        "book_review_link": "",
                        "contributor": "by Paul Kalanithi",
                        "contributor_note": "",
                        "created_date": "2017-09-08 15:00:04",
                        "description": "",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "081298840X",
                        "primary_isbn13": "9780812988406",
                        "publisher": "Random House",
                        "rank": 5,
                        "rank_last_week": 3,
                        "sunday_review_link": "",
                        "title": "WHEN BREATH BECOMES AIR",
                        "updated_date": "2017-09-08 15:00:04",
                        "weeks_on_list": 0,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "http://www.amazon.com/When-Breath-Becomes-Paul-Kalanithi-ebook/dp/B00XSSYR50?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780812988406?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780812988406"
                            }
                        ]
                    }
                ]
            },
            {
                "list_id": 538,
                "list_name": "Sports",
                "list_name_encoded": "sports",
                "display_name": "Sports and Fitness",
                "updated": "MONTHLY",
                "list_image": "https://s1.nyt.com/du/books/images/9780316356541.jpg",
                "list_image_width": 321,
                "list_image_height": 495,
                "books": [
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/What-Made-Maddy-Run-All-American/dp/0316356549?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Kate Fagan",
                        "book_image": "https://s1.nyt.com/du/books/images/9780316356541.jpg",
                        "book_image_width": 321,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by Kate Fagan",
                        "contributor_note": "",
                        "created_date": "2017-09-08 15:00:03",
                        "description": "",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "0316356549",
                        "primary_isbn13": "9780316356541",
                        "publisher": "Little, Brown",
                        "rank": 1,
                        "rank_last_week": 0,
                        "sunday_review_link": "",
                        "title": "WHAT MADE MADDY RUN",
                        "updated_date": "2017-09-08 15:00:03",
                        "weeks_on_list": 0,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/What-Made-Maddy-Run-All-American/dp/0316356549?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780316356541?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780316356541"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "http://www.amazon.com/The-Boys-Boat-Americans-Olympics-ebook/dp/B00AEBETU2?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Daniel James Brown",
                        "book_image": "https://s1.nyt.com/du/books/images/9781101622742.jpg",
                        "book_image_width": 323,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by Daniel James Brown",
                        "contributor_note": "",
                        "created_date": "2017-09-08 15:00:03",
                        "description": "",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "0143125478",
                        "primary_isbn13": "9780143125471",
                        "publisher": "Penguin",
                        "rank": 2,
                        "rank_last_week": 0,
                        "sunday_review_link": "",
                        "title": "THE BOYS IN THE BOAT",
                        "updated_date": "2017-09-08 15:00:03",
                        "weeks_on_list": 0,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "http://www.amazon.com/The-Boys-Boat-Americans-Olympics-ebook/dp/B00AEBETU2?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780143125471?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780143125471"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "https://www.amazon.com/Finding-Gobi-Little-Very-Heart/dp/0718098579?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Dion Leonard with with Craig Borlase",
                        "book_image": "https://s1.nyt.com/du/books/images/9780718098575.jpg",
                        "book_image_width": 325,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by Dion Leonard with with Craig Borlase",
                        "contributor_note": "",
                        "created_date": "2017-09-08 15:00:03",
                        "description": "",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "0718098579",
                        "primary_isbn13": "9780718098575",
                        "publisher": "Thomas Nelson",
                        "rank": 3,
                        "rank_last_week": 0,
                        "sunday_review_link": "",
                        "title": "FINDING GOBI",
                        "updated_date": "2017-09-08 15:00:03",
                        "weeks_on_list": 0,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "https://www.amazon.com/Finding-Gobi-Little-Very-Heart/dp/0718098579?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780718098575?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780718098575"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "http://www.amazon.com/Barbarian-Days-A-Surfing-Life/dp/1511308702?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "William Finnegan",
                        "book_image": "https://s1.nyt.com/du/books/images/9781594203473.jpg",
                        "book_image_width": 309,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by William Finnegan",
                        "contributor_note": "",
                        "created_date": "2017-09-08 15:00:03",
                        "description": "",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "",
                        "primary_isbn13": "9780698163744",
                        "publisher": "Penguin",
                        "rank": 4,
                        "rank_last_week": 0,
                        "sunday_review_link": "https://www.nytimes.com/2015/07/19/books/review/barbarian-days-a-surfing-life-by-william-finnegan.html",
                        "title": "BARBARIAN DAYS",
                        "updated_date": "2017-09-08 15:00:03",
                        "weeks_on_list": 0,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "http://www.amazon.com/Barbarian-Days-A-Surfing-Life/dp/1511308702?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9780698163744?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9780698163744"
                            }
                        ]
                    },
                    {
                        "age_group": "",
                        "amazon_product_url": "http://www.amazon.com/Shoe-Dog-Memoir-Creator-Nike-ebook/dp/B0176M1A44?tag=NYTBS-20",
                        "article_chapter_link": "",
                        "author": "Phil Knight",
                        "book_image": "https://s1.nyt.com/du/books/images/9781501135934.jpg",
                        "book_image_width": 328,
                        "book_image_height": 495,
                        "book_review_link": "",
                        "contributor": "by Phil Knight",
                        "contributor_note": "",
                        "created_date": "2017-09-08 15:00:03",
                        "description": "",
                        "first_chapter_link": "",
                        "price": 0,
                        "primary_isbn10": "1501135910",
                        "primary_isbn13": "9781501135910",
                        "publisher": "Scribner",
                        "rank": 5,
                        "rank_last_week": 0,
                        "sunday_review_link": "",
                        "title": "SHOE DOG",
                        "updated_date": "2017-09-08 15:00:03",
                        "weeks_on_list": 0,
                        "buy_links": [
                            {
                                "name": "Amazon",
                                "url": "http://www.amazon.com/Shoe-Dog-Memoir-Creator-Nike-ebook/dp/B0176M1A44?tag=NYTBS-20"
                            },
                            {
                                "name": "Local Booksellers",
                                "url": "http://www.indiebound.org/book/9781501135910?aff=NYT"
                            },
                            {
                                "name": "Barnes and Noble",
                                "url": "http://www.anrdoezrs.net/click-7990613-11819508?url=http%3A%2F%2Fwww.barnesandnoble.com%2Fw%2F%3Fean%3D9781501135910"
                            }
                        ]
                    }
                ]
            }
        ]
    }
};

// This will output the book title to the console
// console.log(nyt.results.lists[2].books[0].title);

// This will output the book description to the console
// console.log(nyt.results.lists[2].books[0].description);

console.log("Category name"); 
for (category in nyt.results.lists) {
  console.log(nyt.results.lists[category].list_name);
  console.log("--------------");
  for (book in nyt.results.lists[category].books) {
    console.log(nyt.results.lists[category].books[book].title + " by "
      + nyt.results.lists[category].books[book].author);
    console.log("ISBN13: " 
      + nyt.results.lists[category].books[book].primary_isbn13);
    console.log("---Description---");
    console.log(nyt.results.lists[category].books[book].description);
    console.log("--------------");
  }
  console.log("==============");
}
  

